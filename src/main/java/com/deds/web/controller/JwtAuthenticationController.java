package com.deds.web.controller;

import com.deds.web.Security.JwtTokenUtil;
import com.deds.web.Security.UserDetailsServiceImpl;
import com.deds.web.model.JwtRequest;
import com.deds.web.model.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@PostMapping(value = "/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		System.out.println(authenticationRequest);
		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}
	@GetMapping(value = "/z")
	public ResponseEntity<?> createzAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

//		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		System.out.println("authenticationRequest");
//		final UserDetails userDetails = userDetailsService
//				.loadUserByUsername(authenticationRequest.getUsername());
//
//		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse("token"));
	}
	@PostMapping(value = "/open")
	public ResponseEntity<?> createzzAuthenticationToken(@RequestBody Map<String,String> authenticationRequest) throws Exception {

//		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		System.out.println("authenticationRequest open");
		System.out.println(authenticationRequest);
//		final UserDetails userDetails = userDetailsService
//				.loadUserByUsername(authenticationRequest.getUsername());
//
//		final String token = jwtTokenUtil.generateToken(userDetails);
//		new JwtResponse("
		return ResponseEntity.ok(authenticationRequest);
	}


	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}