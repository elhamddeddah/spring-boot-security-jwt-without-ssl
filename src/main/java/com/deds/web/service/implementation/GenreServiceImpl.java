package com.deds.web.service.implementation;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.deds.web.entity.Genre;
import com.deds.web.exception.NotFoundException;
import com.deds.web.repository.GenreRepository;
import com.deds.web.service.GenreService;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class GenreServiceImpl implements GenreService {

	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	@Autowired
	private GenreRepository genreRepository;

	@Override
	public Genre getGenre(long GenreId)
	{
		return (genreRepository.findById(GenreId)
				.orElseThrow(() -> new NotFoundException("Genre", "Not found")));
	}

	@Override
	public void deleteGenre(long GenreId)
	{
		Genre prod=(genreRepository.findById(GenreId)
				.orElseThrow(() -> new NotFoundException("Genre ", "Not found")));

		if(prod.getId().equals(GenreId))
		{genreRepository.delete(prod);}

	}

	@Override
	public List<Genre> getAllGenre() {
		return genreRepository.findAll(Sort.by("createDateTime").descending());
	}



	@Override
	public Genre updateGenre(Genre genre)
	{
		Genre prod=(genreRepository.findById(genre.getId()))
				.orElseThrow(() -> new NotFoundException("Genre", "Not found"));

		if(prod.getId().equals(genre.getId()))
		{return genreRepository.save(genre);}
		else throw new RuntimeException("Erreur de modification de Genre");

	}

	@Override
	public Genre createGenre(Genre genre) {
		return genreRepository.save(genre);
	}

	



}

