package com.deds.web.service.implementation;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.deds.web.entity.Post;
import com.deds.web.exception.NotFoundException;
import com.deds.web.repository.PostRepository;
import com.deds.web.service.PostService;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class PostServiceImpl implements PostService {

	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	@Autowired
	private PostRepository postRepository;

	@Override
	public Post getPost(long PostId)
	{
		return (postRepository.findById(PostId)
				.orElseThrow(() -> new NotFoundException("Post", "Not found")));
	}

	@Override
	public void deletePost(long PostId)
	{
		Post prod=(postRepository.findById(PostId)
				.orElseThrow(() -> new NotFoundException("Post ", "Not found")));

		if(prod.getId().equals(PostId))
		{postRepository.delete(prod);}

	}

	@Override
	public List<Post> getAllPost() {
		return postRepository.findAll(Sort.by("createDateTime").descending());
	}



	@Override
	public Post updatePost(Post post)
	{
		Post prod=(postRepository.findById(post.getId()))
				.orElseThrow(() -> new NotFoundException("Post", "Not found"));

		if(prod.getId().equals(post.getId()))
		{return postRepository.save(post);}
		else throw new RuntimeException("Erreur de modification de Post");

	}

	@Override
	public Post createPost(Post post) {
		return postRepository.save(post);
	}

	



}
