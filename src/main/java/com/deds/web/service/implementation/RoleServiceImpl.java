package com.deds.web.service.implementation;


import com.deds.web.entity.Role;
import com.deds.web.exception.NotFoundException;
import com.deds.web.repository.RoleRepository;
import com.deds.web.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role fetchRoleById(long roleId)
	{
		return (roleRepository.findById(roleId)
				.orElseThrow(() -> new NotFoundException("role","Role inexistant")));
	}

	@Override
	public void deleteRoleById(long roleId)
	{
		Role role=(roleRepository.findById(roleId)
				.orElseThrow(() -> new NotFoundException("role ","Role inexistant")));

		if(role.getRole_id().equals(roleId))
		{roleRepository.delete(role);}

	}

	@Override
	public List<Role> fetchAllRole() {
		return roleRepository.findAll();
	}

	@Override
	public Role updateRole(Role role)
	{
		Role role_=(roleRepository.findById(role.getRole_id())
				.orElseThrow(() -> new NotFoundException("role ","Role inexistant")));

		if(role_.getRole_id().equals(role.getRole_id()))
		{return roleRepository.save(role);}
		else throw new RuntimeException("Erreur de modification de Role");

	}

	@Override
	public Map<String, String> createRole(Role role) {
		return Collections.singletonMap("Id_Role", this.roleRepository.save(role).getRole_id().toString());
	}
}
