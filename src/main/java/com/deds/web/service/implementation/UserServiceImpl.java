package com.deds.web.service.implementation;


import com.deds.web.entity.User;
import com.deds.web.exception.NotFoundException;
import com.deds.web.repository.UserRepository;
import com.deds.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;


	@Override
	public User fetchUserById(long userId)
	{
		return (userRepository.findById(userId)
				.orElseThrow(() -> new NotFoundException("user", "Utilisateur inexistant")));
	}

	@Override
	public void deleteUserById(long userId)
	{
		User user=(userRepository.findById(userId)
				.orElseThrow(() -> new NotFoundException("user ", "Utilisateur inexistant")));

		if(user.getUser_id().equals(userId))
		{userRepository.delete(user);}

	}

	@Override
	public List<User> fetchAllUser() {
		return userRepository.findAll();
	}
	@Override
	public List<User> fetchUserbyRole(String role){
		List<User> users=userRepository.findAll();
		List<User> filtredusers = new ArrayList<>();
//		for (User user : users){
//		 if(utils.UserHasRole(user.getRoles() ,role))
//			 filtredusers.add(user);
//		}
		return filtredusers;
	}

	@Override
	public User updateUser(User user)
	{
		User user_=(userRepository.findById(user.getUser_id())
				.orElseThrow(() -> new RuntimeException("Utilisateur inexistant")));

		if(user_.getUser_id().equals(user.getUser_id()))
		{return userRepository.save(user);}
		else throw new RuntimeException("Erreur Modification");

	}

	@Override
	public Map<String, String> createUser(User user) {
		return Collections.singletonMap("Id_User", this.userRepository.save(user).getUser_id().toString());
	}

	@Override
	public User getConnectedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return userRepository.findByusername(authentication.getName()).orElseThrow(() -> new NotFoundException("user", "Utilisateur inexistant"));
	}
}
