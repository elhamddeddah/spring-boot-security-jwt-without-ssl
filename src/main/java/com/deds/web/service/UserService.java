package com.deds.web.service;

import com.deds.web.entity.User;

import java.util.List;
import java.util.Map;


public interface UserService {

	User fetchUserById(long userId);
	void deleteUserById(long userId);
	List<User> fetchAllUser();
	List<User> fetchUserbyRole(String role);
	User updateUser(User user);
	Map<String, String> createUser(User user);
	User getConnectedUser();

	
}
