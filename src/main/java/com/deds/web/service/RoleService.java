package com.deds.web.service;

import com.deds.web.entity.Role;


import java.util.List;
import java.util.Map;

public interface RoleService {

	Role fetchRoleById(long roleId);
	void deleteRoleById(long roleId);
	List<Role> fetchAllRole();
	Role updateRole(Role role);
	Map<String, String> createRole(Role role);
	
}
