package com.deds.web.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		prePostEnabled = true,
		securedEnabled = true,
		jsr250Enabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter  {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;


    @Autowired
    private JwtRequestFilter jwtRequestFilter;
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
    private AuthenticationSuccessHandler authenticationSuccessHandler;
    @Autowired
    public WebSecurityConfig(AuthenticationSuccessHandler authenticationSuccessHandler) {
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        // Setting Service to find User in the database.
        // And Setting PassswordEncoder
//        auth.userDetailsService(userDetailsService);

    }


    protected void configure(HttpSecurity http) throws Exception {
/*
     //  http.csrf().disable();       //http.authorizeRequests().antMatchers("/**").denyAll();
        http.authorizeRequests()
           //     .antMatchers("/**").permitAll()
                // The pages does not require login
         .antMatchers("/assets/**", "/images/**","/script/**","/style/**","/css/**", "/js/**").permitAll()
//                // The pages does not require login
         .antMatchers("/authenticate","/login", "/logout").permitAll()
//                // /userInfo page requires login as ROLE_USER or ROLE_ADMIN.
//                // If no login, it will redirect to /login page.
         .antMatchers("/post/**","/type/**")
//                        .access("hasAnyRole('ROLE_ADMIN' ,'ROLE_VALIDATION','ROLE_RH','ROLE_LOGISTIQUE')")
                .access("hasAnyRole('ROLE_ADMIN' ,'ROLE_RH','ROLE_USER')")
//         .antMatchers("/alimentation/**","/type/**","/product/**")
//                        .access("hasAnyRole('ROLE_ADMIN' ,'ROLE_RH','ROLE_ALIMENTATION')")


////        http.authorizeRequests().antMatchers("/mail/**","/user/**").access("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')");
//                // For ADMIN only.
////        http.authorizeRequests().antMatchers("/company/**").access("hasRole('ROLE_ADMIN')");
//                .antMatchers("/company/**").access("hasRole('ROLE_ADMIN')")
//                //
                .anyRequest().authenticated();
        // When the user has logged in as XX.
        // But access a page that requires role YY,
        // AccessDeniedException will be thrown.
        http.authorizeRequests().and().exceptionHandling().accessDeniedPage("/403.html");
        // Config for Login Form
        http.authorizeRequests().and().formLogin()//
                // Submit URL of login page.
                .loginPage("/login")//
                .loginProcessingUrl("/loginIn") // Submit URL
              //  .defaultSuccessUrl("/order/")//
                .successHandler(authenticationSuccessHandler)//
                .failureUrl("/login?error=true")//
                .usernameParameter("username")//
                .passwordParameter("password")
                // Config for Logout Page
                .and().logout().logoutUrl("/logout").logoutSuccessUrl("/login")
                ;
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

        // Config Remember Me.
        http.authorizeRequests().and() //
                .rememberMe()
                .rememberMeParameter("remember-me")
                .tokenRepository(this.persistentTokenRepository()) //
                .tokenValiditySeconds(1 * 24 * 60 * 60); //
        System.out.println(http.toString());
*/
        http.csrf().disable()
                // dont authenticate this particular request
                .authorizeRequests().antMatchers("/authenticate", "/register","/open").permitAll().
                // all other requests need to be authenticated
                        anyRequest().authenticated().and().
                // make sure we use stateless session; session won't be used to
                // store user's state.
                        exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Add a filter to validate the tokens with every request
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        //db.setCreateTableOnStartup(true);
        db.setDataSource(dataSource);
        return db;
    }

}