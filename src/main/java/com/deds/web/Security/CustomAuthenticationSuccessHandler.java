package com.deds.web.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@Configuration
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {


    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        String CTXPATH=httpServletRequest.getContextPath();
//        System.out.println("CONTEXT: "+CTXPATH);
        if (roles.contains("ROLE_ALIMENTATION")) {
            httpServletResponse.sendRedirect(CTXPATH+"/alimentation/");
        }
        if (roles.contains("ROLE_ADMIN")) {
            httpServletResponse.sendRedirect(CTXPATH+"/search/results");
        }
        else {
            httpServletResponse.sendRedirect(CTXPATH+"/order/?etat=orderEncours");
        }
    }
}
