// 
// Decompiled by Procyon v0.5.36
// 

package com.deds.web.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer
{
    
    public void addViewControllers(final ViewControllerRegistry registry) {
      //  registry.addViewController("/404.html").setViewName("404");
        registry.addViewController("/403.html").setViewName("error_Page/403");
        registry.addViewController("/404.html").setViewName("error_Page/404");
       // registry.addViewController("/405.html").setViewName("405");
    }
}
