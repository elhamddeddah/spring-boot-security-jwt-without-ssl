package com.deds.web.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Mail {
    private String to;
    private String from;
    private String body;
    private List<String> attachementsFiles;
    private String subject;
    private String htmlfile="<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
            "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
            "    <title>Document</title>\n" +
            "<style>\n" +
            ".customers {\n" +
            "  font-family: Arial, Helvetica, sans-serif;\n" +
            "  border-collapse: collapse;\n" +
            "  width: 100%;\n" +
            "}\n" +
            "\n" +
            ".customers td, .customers th {\n" +
            "  border: 1px solid #ddd;\n" +
            "  padding: 8px;\n" +
            "}\n" +
            "\n" +
            ".customers tr:nth-child(even){background-color: #eee;}\n" +
            "\n" +
            ".customers tr:hover {background-color: #ddd;}\n" +
            "\n" +
            ".customers th {\n" +
            "  padding-top: 12px;\n" +
            "  padding-bottom: 12px;\n" +
            "  text-align: left;\n" +
            "  background-color: FFF000;\n" +
            "  color: white;\n" +
            "}\n" +
            "</style>\n" +
            "</head>\n" +
            "<body>\n" +
            "    \n FFF000"  +
            "</body>\n" +
            "</html>";
    private String td="<td>FFF000</td>";
    private String tr="<tr>FFF000</tr>";
    private String hello=  "<br> <br>Bonjour   FFF000 <br><br> FFF000<br> <br>";
    private String link=   "<br> <br> <button style=\"position: relative;bottom: 5%;left: 10%;background-color: chartreuse;color: ghostwhite;\"> <a href=\"http://172.25.25.16/stock/\">Accéder à l\'application </a></button>";
    private String tableOrder="<table class=\"customers\" >\n" +
            " <thead>\n" +
            "  <tr style=\"background-color: beige;color:#336699\">\n" +
            "    <th>Référence</th>\n" +
            "    <th>Demandé par</th>\n" +
            "    <th>Article</th>\n" +
            "    <th>Quantité</th>\n" +
            "   </tr>\n" +
            "</thead>\n" +
            "<tbody>\n" +
            "    \n FFF000"  +
            "</tbody>\n" +
            "</table>";
    private String tableProduct="<table class=\"customers\" >\n" +
            " <thead>\n" +
            "  <tr >\n" +
            "    <th >Nom </th>\n" +
            "    <th >Description</th>\n" +
            "    <th >Quantité </th>\n" +
            "    <th  >Seuil</th>\n" +
            "    <th >Fournisseur</th>\n" +
            "    <th >Type</th>\n" +
            "   </tr>\n" +
            "</thead>\n" +
            "<tbody>\n" +
            "    \n FFF000"  +
            "</tbody>\n" +
            "</table>";

    @Override
    public String toString() {
        return "<br>Mail<br>" +
                "<br>to      :  "+to  +
                "<br>from    :  " + from +
                "<br>subject :  " + subject +
                "<br>body    :  " + body +
                "<br>";
    }


}
