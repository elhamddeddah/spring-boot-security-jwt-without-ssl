package com.deds.web.model;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@Data
@Builder



public class Orderfiltered {
    private String agence;
    private String direction;
    private String product;
    private int qte;
    private String details;
}
