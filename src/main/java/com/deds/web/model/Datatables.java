package com.deds.web.model;

import java.time.LocalDateTime;
import java.util.List;

import com.deds.web.entity.Genre;
import com.deds.web.entity.Post;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Datatables {

    private int draw;
    private int start;
    private long recordsTotal;
    private long recordsFiltered;
    private List<Post> data;


    // setter and getter ...

}
