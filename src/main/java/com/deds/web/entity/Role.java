package com.deds.web.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;


import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity

@Table(name = "Market_roles")
public class Role {
    @Id
//    @GenericGenerator(name = "Market_ROLES_SEQUENCE", strategy="org.hibernate.id.enhanced.SequenceStyleGenerator",
//            parameters ={
//                    @org.hibernate.annotations.Parameter(name="Market_ROLES_SEQUENCE",value="Market_ROLES_SEQUENCE"),
//                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
//                    @org.hibernate.annotations.Parameter(name="increment_size",value="1")
//            })
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "role_id")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long role_id;
    private String name;
    @Builder.Default
    @Column(name = "nom")
    private String  nom="a";





}