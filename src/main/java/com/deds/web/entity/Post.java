package com.deds.web.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.GenericGenerator;





@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity

@Table(name = "Market_POST")
public class Post {
    @Id
//    @GenericGenerator(name = "Market_POST_SEQUENCE", strategy="org.hibernate.id.enhanced.SequenceStyleGenerator",
//            parameters ={
//                    @org.hibernate.annotations.Parameter(name="Market_POST_SEQUENCE",value="Market_POST_SEQUENCE"),
//                    @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"),
//                    @org.hibernate.annotations.Parameter(name="increment_size",value="1")
//            })
//  @GeneratedValue( generator = "Market_POST_SEQUENCE")
    @Column(name = "post_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String tel;
    private String photos;
    private String detail;
    @ManyToOne
    @JoinColumn( name="genre_id" )
    private Genre genre;
    private int price;
    private int qte;
    private int likes;
    private int oldprice;
    private boolean isNew;
    @CreationTimestamp
    @Column(name = "createDateTime", nullable = false, updatable = false)
    private LocalDateTime createDateTime;
    @UpdateTimestamp
    private LocalDateTime updateDateTime;

}
